struct Band
    name::String
    origin::String
    genre::String
    members::Array
    start_year::Integer

    Band(name, origin, genre, members; start_year=-9999) = new(
        name, origin, genre, members, start_year
    )
    Band(name, origin, genre, members, start_year) = new(
        name, origin, genre, members, start_year
    )
end

methods(Band)

dreamtheater = Band(
    "Dream Theater",
    "USA",
    "Progressive Metal/Rock",
    ["James L.", "John P.", "John M.", "Mike P.", "Jordan R."],
    1985
)

dreamtheater = Band(
    "Dream Theater",
    "USA",
    "Progressive Metal/Rock",
    ["James L.", "John P.", "John M.", "Mike P.", "Jordan R."]
)
