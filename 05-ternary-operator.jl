systematic_chaos = 4.1
paranoid = 4.7

function rating_to_label(r::Float64)
    if r >= 4.0
        if r >= 4.5
            return "Fantastic"
        end
        return "Very Good"
    end
    "Unknown"
end

rating_to_label(systematic_chaos)
rating_to_label(paranoid)

# condition ? satisfied : not satisfied

function rating_to_label_ternary(r::Float64)
    r >= 4.0 ? r >= 4.5 ? "Fantastic" : "Very Good" : "Unknown"
end

rating_to_label_ternary(systematic_chaos)
rating_to_label_ternary(paranoid)
























struct Band
    name::String
    origin::String
    genre::String
    members::Array
    start_year::Integer

    Band(name, origin, genre, members; start_year=-9999) = new(
        name, origin, genre, members, start_year
    )
    Band(name, origin, genre, members, start_year) = new(
        name, origin, genre, members, start_year
    )

end

dreamtheater = Band(
    "Dream Theater",
    "USA",
    "Progressive Metal/Rock",
    ["James L.", "John P.", "John M.", "Mike P.", "Jordan R."],
    1985
)

blacksabbath = Band(
    "Black Sabbath",
    "UK",
    "Heavy Metal",
    ["Tony I.", "Bill W.", "Geezer B.", "Ozzy O."],
    1968
)
