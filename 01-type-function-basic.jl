a_text = "A text"
a_float = 1.5

an_array = [a_text, a_float]

an_array_with_type = Array{String}(undef, 10)

a_dict = Dict("a" => 1, "b" => 2)

a_dict_with_type = Dict{String, Union{String, Integer}}()

a_dict_with_type["a"] = 1

a_dict_with_type["b"] = "String"

# typeof(a_dict)

do_square(x) = x*x

do_int_square(x::Integer) = x*x

do_int_square(1)

2 |> do_int_square |> do_square

function do_square_long(x)
    x * x
end

do_square_long(2)


function about_me(name::String, age::Integer, height::Float64)
    println("My name is $name, I am $age years old and I am $height cm tall.")
end

about_me("DataPsycho", 30, 168.0)

function about_me(; name::String, age=-999, height=-999.0)
    println("My name is $name, I am $age years old and I am $height cm tall.")
end

about_me(name="DataPsycho", height=168.0, age=30)

function about_me(name::String; age=-999, height=-999.0)
    println("My name is $name, I am $age years old and I am $height cm tall.")
end

about_me("DataPsycho", height=168.0, age=30)
