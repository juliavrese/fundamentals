# Semantic Versioning
Semantic Versioning is a way to version software with 3 main keyword chaining with `.`. For example `1.1.1`, can be expressed as `Major.Minor.Patch.

## When to Update which section?
If there is some bug fix or small performance optimization then the patch version should be changed. If we have added some new function but the older functions works as it is then minor version should be changed. If we think the new version has some change which is not compatible with older version any more then the Major version should be change.
