struct Album
    album_name::String
    genre::String
    published::Integer
    artist::String
    total_songs::Integer
    rating::Float64
end

systemetic_chaos = Album("Systemetic Chaos", "Metal", 2007, "Dream Theater", 8, 4.1)
paranoid = Album("Paranoid", "Metal", 1970, "Black Sabbath", 8, 4.7)
steelheart = Album("Steelheart", "Rock", 1991, "Steelheart", 8, 4.4)
nevermind = Album("Nevermind", "Rock", 1991, "Nirvana", 13, 4.8)

album_bucket = [systemetic_chaos, paranoid, steelheart, nevermind]

# Task:
# Create a Mix Album Bundle with any 2 Album
# If If Both has same Genre then label the bundle as <Genre> Mix
# If both album in the bundle does not have same genre name the bundle as <Genre> & <Genre> Mix

rand(album_bucket, 2)

function make_bundle(ab::Array)
    bundle = rand(ab, 2)
    album_1 = bundle[1]
    album_2 = bundle[2]
    if album_1.genre == "Metal" && album_2.genre == "Metal"
        return bundle, "Metal Mix Bundle"
    elseif album_1.genre == "Rock" && album_2.genre == "Rock"
        return bundle, "Rock Mix Bundle"
    else
        return bundle, "Rock & Metal Mix Bundle"
    end
end


make_bundle(album_bucket)
