struct Band
    name::String
    origin::String
    genre::String
    members::Array
    start_year::Integer

    Band(name, origin, genre, members; start_year=-9999) = new(
        name, origin, genre, members, start_year
    )
    Band(name, origin, genre, members, start_year) = new(
        name, origin, genre, members, start_year
    )
end

struct Album
    album_name::String
    genre::String
    published::Integer
    artist::String
    total_songs::Integer
    rating::Float64
end

# Create A composotion using Band and Album
struct Album
    album_name::String
    genre::String
    published::Integer
    artist::Band
    total_songs::Integer
    rating::Float64
end


function get_name(b::Band)
    return b.name
end

function get_name(a::Album)
    return a.album_name
end

dreamtheater = Band(
    "Dream Theater",
    "USA",
    "Progressive Metal/Rock",
    ["James L.", "John P.", "John M.", "Mike P.", "Jordan R."],
    1985
)

systemetic_chaos = Album("Systemetic Chaos", "Metal", 2007, "Dream Theater", 8, 4.1)

get_name(dreamtheater)
get_name(systemetic_chaos)


dt_dict = Dict(:name => "Dream Theater")
sc_dict Dict(:album_name => "Systemetic Chaos")

function get_name(d::Dict)
    return d[:name]
end

function get_name_album(d::Dict)
    return d[:album_name]
end
