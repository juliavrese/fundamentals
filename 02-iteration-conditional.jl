a_list = ["James", "John", "Tim", "Data P."]

for (i, each_item) in enumerate(a_list)
    if lowercase(each_item) == "tim"
        deleteat!(a_list, i)
    else
        println(i, " ", lowercase(each_item))
    end
end
