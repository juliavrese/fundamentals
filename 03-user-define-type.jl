struct Band
    name::String
    origin::String
    genre::String
    members::Array
    start_year::Integer
end

dreamtheater = Band(
    "Dream Theater",
    "USA",
    "Progressive Metal/Rock",
    ["James L.", "John P.", "John M.", "Mike P.", "Jordan R."],
    1985
)

function remove_member!(b::Band, m::String)
    for (i, each_member) in enumerate(b.members)
        if lowercase(each_member) == lowercase(m)
            deleteat!(b.members, i)
        end
    end
end

add_member!(b::Band, m::String) = push!(b.members, m)

remove_member!(dreamtheater, "Mike P.")

add_member!(dreamtheater, "Mike M.")

dreamtheater.members
